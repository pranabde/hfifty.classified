﻿using HFifty.Classified.SharedKarnel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HFifty.Classified.JobPortal.Domain
{
    public sealed class JobSeeker : Entity<Guid>, IJobSeeker
    {
        #region Scalar Properties
        public string JobSeekerName { get; private set; }
        public long MobileNumber { get; private set; }
        public string EmailAddress { get; private set; }
        #endregion

        #region Navigation Properties
        public Address Address { get; private set; }
        public JobSeekerProfile Profile { get; private set; }
        #endregion

        #region Collection Navigation Properties
        public ICollection<JobApplication> AppliedJobs { get; private set; }
        #endregion

        #region Job Seeker Constructor
        public JobSeeker(string jobSeekerName, long mobileNumber,
                            string emailAddress, string addressOne, string addressTwo,
                            string city, string state, int pinCode) : this()
        {
            Id = Guid.NewGuid();
            JobSeekerName = jobSeekerName;
            MobileNumber = mobileNumber;
            EmailAddress = emailAddress;
            Address = new Address(addressOne, addressTwo, city, state, pinCode);

            AppliedJobs = new List<JobApplication>();
            Profile = new JobSeekerProfile(Id);
        }
        public JobSeeker() : base(Guid.NewGuid())
        {

        }
        #endregion

        #region Public Methods       
        public JobApplication ApplyForNewJob(Job job)
        {
            if (!CheckBasicProfileCompleteness())
            {
                throw new InvalidOperationException("Complete your profile first. Required: ProfileSummary, Technology");
            }
            if (AppliedJobs.Any(a => a.JobTitle == job.JobTitle && a.JobPostedDate == job.PostedDate))
            {
                throw new InvalidOperationException("Already applied for this job");
            }

            var application = new JobApplication(job.Id, Id, job.Provider.Id, JobSeekerName, EmailAddress,
                                                    job.JobTitle, job.PostedDate, Profile)
            {
            };
            application.SetApplicationStatus(JobApplicationStatus.Applied);            
            AppliedJobs.Add(application);
            //TO DO: Notify provider about the new application
            //DomainEvent.Raise(new JobApplicationCreatedNotifier() { JobApplication = application, Provider = job.Provider});
            return application;
        }

        public IEnumerable<JobApplication> GetAllAppliedJobs()
        {
            return AppliedJobs.Where(a => a.JobApplicationStatus != JobApplicationStatus.Settled ||
                                          a.JobApplicationStatus != JobApplicationStatus.Cancelled);
        }

        public JobSeekerProfile UpdateJobProfile(string profileHeadline, string profileSummary,
                                    float experience, IEnumerable<Technology> technologies,
                                    string company, string projectDescription)
        {
            Profile = Profile.UpdateProfile(profileHeadline, profileSummary, experience,
                                                technologies, company, projectDescription);
            return Profile;
        }

        public IEnumerable<JobApplication> ViewAppliedJobs()
        {
            return AppliedJobs;
        }

        public JobSeekerProfile ViewJobProfile()
        {
            return Profile;
        }

        public void WithdrawJobApplication(JobApplication jobApplication)
        {
            if (!AppliedJobs.Any(a => a.Equals(jobApplication)))
            {
                throw new InvalidOperationException("Application not found");
            }
            jobApplication.SetApplicationStatus(JobApplicationStatus.Cancelled);
            AppliedJobs.Remove(jobApplication);
        }
        #endregion

        #region Private Methods
        private bool CheckBasicProfileCompleteness()
        {
            var isProfileComplete = true;
            if (Profile.TechnologyStack.Count == 0 ||
                string.IsNullOrEmpty(Profile.ProfileSummary))
            {
                isProfileComplete = false;
            }
            return isProfileComplete;
        }
        #endregion
    }
}
