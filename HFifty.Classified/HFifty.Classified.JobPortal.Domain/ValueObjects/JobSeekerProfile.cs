﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HFifty.Classified.JobPortal.Domain
{
    public class JobSeekerProfile
    {
        public JobSeekerProfile(Guid jobSeekerId)
        {
            Id = jobSeekerId;
            ProfileSummary = string.Empty;
            Experience = 0.00F;
            ProfileHeadline = string.Empty;
            CurrentCompanyName = string.Empty;
            ProjectDescription = string.Empty;
            TechnologyStack = new List<Technology>();
        }
        private JobSeekerProfile()
        {

        }
        public Guid Id { get; private set; }
        public string ProfileSummary { get; private set; }
        public float Experience { get; private set; }
        public string ProfileHeadline { get; private set; }
        public ICollection<Technology> TechnologyStack { get; private set; }
        public string CurrentCompanyName { get; private set; }
        public string ProjectDescription { get; private set; }

        internal JobSeekerProfile UpdateProfile(string profileHeadline, string profileSummary, float experience,
                                    IEnumerable<Technology> technologies, string company, string projectDescription)
        {
            ProfileHeadline = profileHeadline;
            ProfileSummary = profileSummary;
            Experience = experience;
            if (technologies.Any())
            {
                TechnologyStack.Clear();
                foreach (var tech in technologies)
                {
                    TechnologyStack.Add(tech);
                }
            }
            CurrentCompanyName = company;
            ProjectDescription = projectDescription;
            return this;
        }

        public override bool Equals(object obj)
        {
            return obj != null
                && obj.GetType() == typeof(JobSeekerProfile)
                && Id == ((JobSeekerProfile)obj).Id;
        }

        public override int GetHashCode()
        {
            return -1771400962 + EqualityComparer<Guid>.Default.GetHashCode(Id);
        }

        public static bool operator ==(JobSeekerProfile profile1, JobSeekerProfile profile2)
        {
            return EqualityComparer<JobSeekerProfile>.Default.Equals(profile1, profile2);
        }

        public static bool operator !=(JobSeekerProfile profile1, JobSeekerProfile profile2)
        {
            return !(profile1 == profile2);
        }
    }
}
