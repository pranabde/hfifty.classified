﻿using HFifty.Classified.SharedKarnel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFifty.Classified.UserService.Domain
{
    public class UserAddedOrUpdated : IDomainEvent
    {
        public string UserName { get; set; }
        public long MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public Address Address { get; set; }
        public UserType UserType { get; set; }

        public UserAddedOrUpdated(string userName, long mobileNumber,
                                    string emailAddress, Address address, UserType userType)
        {
            UserName = userName;
            MobileNumber = mobileNumber;
            EmailAddress = emailAddress;
            Address = address;
            UserType = userType;
        }
    }
}
