﻿using System.Collections.Generic;

namespace HFifty.Classified.JobPortal.Domain
{
    public interface IJobSeeker
    {
        JobApplication ApplyForNewJob(Job job);
        IEnumerable<JobApplication> GetAllAppliedJobs();
        JobSeekerProfile UpdateJobProfile(string profileHeadline, string profileSummary, float experience, IEnumerable<Technology> technologies, string company, string projectDescription);
        IEnumerable<JobApplication> ViewAppliedJobs();
        JobSeekerProfile ViewJobProfile();
        void WithdrawJobApplication(JobApplication jobApplication);
    }
}