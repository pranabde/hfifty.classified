﻿using System;
using System.Collections.Generic;
using HFifty.Classified.SharedKarnel;

namespace HFifty.Classified.JobPortal.Domain
{
    public interface IJobProvider
    {
        Job CreateOrUpdateJob(string jobTitle, string jobDescription, double hourlyPay, string companyName, string city, DateTime postedDate);
        Job DeleteJob(string jobTitle, DateTime postedDate);
        IEnumerable<JobApplication> GetAllJobApplications(string jobTitle, DateTime postedDate);
        IEnumerable<Job> GetAvailableJobs();
        JobApplication GetJobApplication(string jobTitle, DateTime postedDate, string jobSeekerName, string jobSeekerEmail);
        void RemoveAllJobApplications(string jobTitle, DateTime postedDate);
        void UpdateJobApplicationStatus(string jobTitle, DateTime postedDate, string jobSeekerName, string jobSeekerEmail, JobApplicationStatus applicationStatus);
        JobSeekerProfile ViewApplicantProfile(string jobSeekerName, string jobSeekerEmail);
    }
}