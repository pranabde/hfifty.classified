﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFifty.Classified.SharedKarnel
{
    public static class DomainEvent
    {
        public static IEventDispatcher Dispatcher { get; }
        public static void Raise<T>(T eventToRaise) where T : IDomainEvent
        {
            Dispatcher.Dispatch(eventToRaise);
        }
    }
}
