﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFifty.Classified.SharedKarnel
{
    public static class PasswordEncryptor
    {
        public static string EnocdePassword(string password)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(password);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string DecodePassword(string passPhrase)
        {
            var base64EncodedBytes = Convert.FromBase64String(passPhrase);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
