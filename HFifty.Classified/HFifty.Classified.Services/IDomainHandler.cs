﻿namespace HFifty.Classified.SharedKarnel
{
    public interface IDomainHandler<T> where T : IDomainEvent
    {
        void Handle(T eventToHandle);
    }
}
