﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFifty.Classified.SharedKarnel
{
    public enum JobApplicationStatus
    {
        Applied,
        ShortListed,
        Processing,
        Selected,
        Rejected,
        Settled,
        Cancelled
    }
}
