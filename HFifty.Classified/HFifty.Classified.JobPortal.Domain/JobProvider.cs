﻿using HFifty.Classified.SharedKarnel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HFifty.Classified.JobPortal.Domain
{
    public sealed class JobProvider : Entity<Guid>, IJobProvider
    {
        #region Scalar Properties
        public string ProviderName { get; private set; }
        public long MobileNumber { get; private set; }
        public string EmailAddress { get; private set; }
        #endregion

        #region Navigation Properties
        public Address Address { get; private set; }
        #endregion

        #region Collection Navigation Properties
        public ICollection<Job> Jobs { get; private set; }
        public ICollection<JobApplication> JobApplications { get; private set; }
        #endregion

        #region Job Provider Constructor
        public JobProvider(string providerName, long mobileNumber,
                            string emailAddress, string addressOne, string addressTwo,
                            string city, string state, int pinCode) : this()
        {
            Id = Guid.NewGuid();
            ProviderName = providerName;
            MobileNumber = mobileNumber;
            EmailAddress = emailAddress;
            Address = new Address(addressOne, addressTwo, city, state, pinCode);

            Jobs = new List<Job>();
            JobApplications = new List<JobApplication>();
        }

        public JobProvider() : base(Guid.NewGuid())
        {

        }
        #endregion

        #region Public Methods      
        public Job CreateOrUpdateJob(string jobTitle, string jobDescription, double hourlyPay, string companyName, string city, DateTime postedDate)
        {
            Job job = Jobs.Where(j => string.Equals(j.JobTitle, jobTitle,
                                                            StringComparison.InvariantCultureIgnoreCase) &&
                                                            DateTime.Equals(postedDate, j.PostedDate) &&
                                                            j.Provider.Id == Id &&
                                                            j.Provider.EmailAddress == EmailAddress &&
                                                            j.Provider.MobileNumber == MobileNumber)
                           .FirstOrDefault();

            if (job == null)
            {
                job = new Job(Guid.NewGuid());
                job.CreateJob(jobTitle, jobDescription, hourlyPay, city, postedDate, companyName, this);
            }
            else
            {
                job.UpdateJobDescription(jobDescription, hourlyPay);
            }
            job.MarkJobStatus(true);
            AddToJobsCollection(job);
            return job;
        }

        public Job DeleteJob(string jobTitle, DateTime postedDate)
        {
            var targetJob = Jobs.Where(a => a.JobTitle == jobTitle &&
                                            a.PostedDate == postedDate)
                                .FirstOrDefault();
            if (targetJob == null)
            {
                throw new InvalidOperationException("Job not found");
            }
            RemoveAllJobApplications(jobTitle, postedDate);
            Jobs.Remove(targetJob);
            return targetJob;
        }

        public IEnumerable<Job> GetAvailableJobs()
        {
            return Jobs.Where(j => j.IsActiveJob);
        }

        public IEnumerable<JobApplication> GetAllJobApplications(string jobTitle, DateTime postedDate)
        {
            var targetJob = Jobs.Where(a => a.JobTitle == jobTitle &&
                                            a.PostedDate == postedDate)
                                .FirstOrDefault();
            if (targetJob == null)
            {
                throw new InvalidOperationException("Job not found");
            }
            var targetApplications = JobApplications.Where(a => a.JobId == targetJob.Id &&
                                                            (a.JobApplicationStatus != JobApplicationStatus.Cancelled ||
                                                            a.JobApplicationStatus != JobApplicationStatus.Settled)).ToList();

            return targetApplications;
        }

        public JobApplication GetJobApplication(string jobTitle, DateTime postedDate, string jobSeekerName, string jobSeekerEmail)
        {
            var targetApplications = GetAllJobApplications(jobTitle, postedDate);
            var targetApplication = targetApplications.Where(a => a.JobSeekerName == jobSeekerName &&
                                                                    a.JobSeekerEmail == jobSeekerEmail)
                                                      .SingleOrDefault();
            if (targetApplication == null)
            {
                throw new InvalidOperationException("Job Application not found");
            }
            return targetApplication;
        }

        public void RemoveAllJobApplications(string jobTitle, DateTime postedDate)
        {
            var targetApplications = GetAllJobApplications(jobTitle, postedDate);
            if (targetApplications.Any())
            {
                foreach (var application in targetApplications)
                {
                    JobApplications.Remove(application);
                }
            }
        }

        public void UpdateJobApplicationStatus(string jobTitle, DateTime postedDate, string jobSeekerName, string jobSeekerEmail, JobApplicationStatus applicationStatus)
        {
            var targetApplication = GetJobApplication(jobTitle, postedDate, jobSeekerName, jobSeekerEmail);
            if (applicationStatus != targetApplication.JobApplicationStatus &&
                (applicationStatus != JobApplicationStatus.Cancelled ||
                 applicationStatus != JobApplicationStatus.Settled))
            {
                targetApplication.SetApplicationStatus(applicationStatus);
            }
        }

        public JobSeekerProfile ViewApplicantProfile(string jobSeekerName, string jobSeekerEmail)
        {
            var jobApplication = JobApplications.Where(a => a.JobSeekerName == jobSeekerName &&
                                                             a.JobSeekerEmail == jobSeekerEmail)
                                                 .FirstOrDefault();
            if (jobApplication == null)
            {
                throw new InvalidOperationException("Job Application not found");
            }
            return jobApplication.GetApplicantProfile();
        }
        #endregion

        #region Private Methods
        private void AddToJobsCollection(Job job)
        {
            var targetJob = Jobs.Where(j => j.Id == job.Id &&
                                            j.Provider.Id == Id &&
                                            j.Provider.EmailAddress == EmailAddress &&
                                            j.Provider.MobileNumber == MobileNumber)
                                .FirstOrDefault();
            if (targetJob == null)
            {
                Jobs.Add(job);
            }
            else
            {
                Jobs.Remove(targetJob);
                Jobs.Add(job);
            }
        }
        #endregion
    }
}
