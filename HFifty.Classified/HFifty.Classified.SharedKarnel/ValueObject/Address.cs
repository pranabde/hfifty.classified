﻿using System.Collections.Generic;

namespace HFifty.Classified.SharedKarnel
{
    public class Address
    {
        public Address(string addressOne, string addressTwo, string city, string state, int pinCode)
        {
            AddressOne = addressOne;
            AddressTwo = addressTwo;
            City = city;
            State = state;
            PinCode = pinCode;
        }

        public string AddressOne { get; private set; }
        public string AddressTwo { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public int PinCode { get; private set; }

        public override bool Equals(object obj)
        {
            return 
                AddressOne == ((Address)obj).AddressOne &&
                AddressTwo == ((Address)obj).AddressTwo &&
                City == ((Address)obj).City &&
                State == ((Address)obj).State &&
                PinCode == ((Address)obj).PinCode;
        }

        public override int GetHashCode()
        {
            var hashCode = 2022043837;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(AddressOne);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(AddressTwo);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(City);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(State);
            hashCode = hashCode * -1521134295 + PinCode.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(Address address1, Address address2)
        {
            return EqualityComparer<Address>.Default.Equals(address1, address2);
        }

        public static bool operator !=(Address address1, Address address2)
        {
            return !(address1 == address2);
        }
    }
}
