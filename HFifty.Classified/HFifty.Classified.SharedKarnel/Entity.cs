﻿using System;

namespace HFifty.Classified.SharedKarnel
{
    public abstract class Entity<TId> : IEquatable<Entity<TId>>
    {
        public TId Id { get; protected set; }

        protected Entity(TId id)
        {
            if (object.Equals(id, default(TId)))
            {
                throw new ArgumentException("The Id can not be its type's defalut value.");
            }
            else
            {
                this.Id = id;
            }
        }

        protected Entity()
        {

        }

        public bool Equals(Entity<TId> other)
        {
            if (other == null)
            {
                return false;
            }
            else
            {
                return this.Id.Equals(other);
            }
        }

        public override bool Equals(object otherObj)
        {
            var entity = otherObj as Entity<TId>;
            if (entity != null)
            {
                return this.Id.Equals(entity.Id);
            }
            return base.Equals(otherObj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
