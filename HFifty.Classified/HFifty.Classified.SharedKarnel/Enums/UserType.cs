﻿namespace HFifty.Classified.SharedKarnel
{
    public enum UserType
    {
        JobProvider,
        ServiceProvider,
        ProductInventoryRepresentative,
        JobApplicant,
        ProductCustomer,
        ServiceConsumer
    }
}
