﻿using HFifty.Classified.SharedKarnel;
using System;

namespace HFifty.Classified.JobPortal.Domain
{
    public class Job : Entity<Guid>
    {
        public Job(Guid jobId) : base(jobId)
        {

        }
        private Job()
        {

        }
        public Guid JobId { get; private set; }
        public JobProvider Provider { get; private set; }
        public string JobTitle { get; private set; }
        public DateTime PostedDate { get; private set; }
        public string JobDescription { get; private set; }
        public string CompanyName { get; private set; }
        public bool IsActiveJob { get; private set; }
        public double HourlyPay { get; private set; }
        public string City { get; private set; }

        internal void MarkJobStatus(bool status)
        {
            IsActiveJob = status;
        }

        internal void CreateJob(string jobTitle, string jobDescription, double hourlyPay, 
                string city, DateTime postedDate, string companyName, JobProvider jobProvider)
        {
            JobTitle = jobTitle;
            JobDescription = jobDescription;
            HourlyPay = hourlyPay;
            City = city;
            PostedDate = postedDate;
            CompanyName = companyName;
            Provider = jobProvider;
        }

        internal void UpdateJobDescription(string jobDescription, double hourlyPay)
        {
            JobDescription = jobDescription;
            HourlyPay = hourlyPay;
        }
    }
}
