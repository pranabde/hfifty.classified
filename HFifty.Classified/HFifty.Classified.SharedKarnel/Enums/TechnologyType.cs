﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFifty.Classified.SharedKarnel
{
    public enum TechnologyType
    {
        DotNet,
        Java,
        AngularJS,
        Javascript,
        Python,
        Ruby,
        Machine_Learning,
        Data_Science,
        RDBMS,
        Networking,
        Support
    }
}
