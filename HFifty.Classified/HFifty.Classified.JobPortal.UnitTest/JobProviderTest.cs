﻿using System;
using System.Collections.Generic;
using System.Linq;
using HFifty.Classified.JobPortal.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HFifty.Classified.JobPortal.UnitTest
{
    [TestClass]
    public class JobProviderTest
    {
        private IJobProvider _provider;
        private IJobSeeker _seeker;

        [TestInitialize]
        public void TestInitialize()
        {
            _provider = new JobProvider("Pranab", 12345, "pk@g.com",
                                "H50", "Velachery", "Chennai", "TN", 600042);
            _seeker = new JobSeeker("Koustav", 67890, "kd@g.com",
                                "H50", "Velachery", "Chennai", "TN", 600042);
            _seeker.UpdateJobProfile("dotnet developer", "5 years exp. dotnet developer",
                5, new List<Technology> { new Technology { TechnologyType = SharedKarnel.TechnologyType.DotNet } }, "BNP", "PNL");
        }

        [TestMethod]
        public void CreateOrUpdateNewJobTest()
        {
            _provider.CreateOrUpdateJob("Job1", "Desc", 100, "BNP", "Chennai", DateTime.Parse("12 Nov 1990"));
            Assert.IsTrue(_provider.GetAvailableJobs().Count() == 1);
            Assert.AreEqual(_provider.GetAvailableJobs().First().IsActiveJob, true);
            Assert.AreEqual(_provider.GetAvailableJobs().First().JobDescription, "Desc");
            Assert.AreEqual(_provider.GetAvailableJobs().First().JobTitle, "Job1");
            Assert.AreEqual(_provider.GetAvailableJobs().First().PostedDate.Month.ToString(), "11");
        }

        [TestMethod]
        public void DeleteJobTest()
        {
            _provider.CreateOrUpdateJob("Job1", "Desc", 100, "BNP", "Chennai", DateTime.Parse("12 Nov 1990"));
            Assert.IsTrue(_provider.GetAvailableJobs().Count() == 1);
            var actualJob = _provider.GetAvailableJobs().First();
            _provider.DeleteJob(actualJob.JobTitle, actualJob.PostedDate);
            Assert.IsTrue(_provider.GetAvailableJobs().Count() == 0);
        }

        [TestMethod]
        public void GetAllJobApplicationsTest()
        {
            _provider.CreateOrUpdateJob("Job1", "Desc", 100, "BNP", "Chennai", DateTime.Parse("12 Nov 1990"));
            Assert.IsTrue(_provider.GetAvailableJobs().Count() == 1);
            var actualJob = _provider.GetAvailableJobs().First();
            Assert.IsTrue(_provider.GetAllJobApplications(actualJob.JobTitle, actualJob.PostedDate).Count() == 0);
        }

        [TestMethod]
        public void GetJobApplicationsTest()
        {
            _provider.CreateOrUpdateJob("Job1", "Desc", 100, "BNP", "Chennai", DateTime.Parse("12 Nov 1990"));
            var actualJob = _provider.GetAvailableJobs().First();
            Assert.IsTrue(_provider.GetAllJobApplications(actualJob.JobTitle, actualJob.PostedDate).Count() == 0);
            _seeker.ApplyForNewJob(actualJob);
            Assert.IsTrue(_provider.GetAllJobApplications(actualJob.JobTitle, actualJob.PostedDate).Count() == 1);

        }
    }
}
