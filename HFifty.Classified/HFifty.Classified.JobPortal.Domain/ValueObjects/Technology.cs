﻿using HFifty.Classified.SharedKarnel;
using System;
using System.Collections.Generic;

namespace HFifty.Classified.JobPortal.Domain
{
    public class Technology
    {
        public int Id { get; set; }
        public TechnologyType TechnologyType { get; set; }
        public ICollection<JobSeekerProfile> Profiles { get; set; }
    }
}
