﻿using HFifty.Classified.SharedKarnel;
using System;
using System.Collections.Generic;

namespace HFifty.Classified.JobPortal.Domain
{
    public sealed class JobApplication
    {
        public JobApplication(Guid jobId, Guid jobSeekerId, Guid providerId, string jobSeekerName, string emailAddress,
                                string jobTitle, DateTime postedDate, JobSeekerProfile jobSeekerProfile)
        {
            Id = Guid.NewGuid();
            JobId = jobId;
            JobSeekerId = jobSeekerId;
            ProviderId = providerId;
            JobSeekerName = jobSeekerName;
            JobSeekerEmail = emailAddress;
            JobTitle = jobTitle;
            JobPostedDate = postedDate;
            JobSeekerProfile = jobSeekerProfile;
        }

        private JobApplication()
        {

        }
        public Guid Id { get; private set; }
        public Guid JobId { get; private set; }
        public Guid JobSeekerId { get; private set; }
        public Guid ProviderId { get; private set; }
        public string JobSeekerName { get; private set; }
        public string JobSeekerEmail { get; private set; }
        public JobApplicationStatus JobApplicationStatus { get; private set; }
        public string JobTitle { get; private set; }
        public DateTime JobPostedDate { get; private set; }
        public JobSeekerProfile JobSeekerProfile { get; private set; }
        public void SetApplicationStatus(JobApplicationStatus jobApplicationStatus)
        {
            JobApplicationStatus = jobApplicationStatus;
        }

        internal JobSeekerProfile GetApplicantProfile()
        {
            return JobSeekerProfile;
        }

        public override bool Equals(object obj)
        {
            return obj != null
                && obj.GetType() == typeof(JobApplication)
                && JobId == ((JobApplication)obj).JobId
                && JobSeekerId == ((JobApplication)obj).JobSeekerId
                && ProviderId == ((JobApplication)obj).ProviderId
                && JobSeekerName == ((JobApplication)obj).JobSeekerName
                && JobSeekerEmail == ((JobApplication)obj).JobSeekerEmail
                && JobTitle == ((JobApplication)obj).JobTitle
                && JobPostedDate == ((JobApplication)obj).JobPostedDate;
        }

        public override int GetHashCode()
        {
            var hashCode = 177959005;
            hashCode = hashCode * -1521134295 + EqualityComparer<Guid>.Default.GetHashCode(JobId);
            hashCode = hashCode * -1521134295 + EqualityComparer<Guid>.Default.GetHashCode(JobSeekerId);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(JobSeekerName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(JobSeekerEmail);
            hashCode = hashCode * -1521134295 + JobApplicationStatus.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(JobTitle);
            hashCode = hashCode * -1521134295 + JobPostedDate.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<JobSeekerProfile>.Default.GetHashCode(JobSeekerProfile);
            return hashCode;
        }

        public static bool operator ==(JobApplication application1, JobApplication application2)
        {
            return EqualityComparer<JobApplication>.Default.Equals(application1, application2);
        }

        public static bool operator !=(JobApplication application1, JobApplication application2)
        {
            return !(application1 == application2);
        }
    }
}
