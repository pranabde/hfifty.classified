﻿using HFifty.Classified.SharedKarnel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFifty.Classified.UserService.Domain
{
    public sealed class User : Entity<Guid>
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string CompanyName { get; private set; }
        public string CompanyAddress { get; private set; }
        public string UserName { get; private set; }
        public long MobileNumber { get; private set; }
        public string EmailAddress { get; private set; }
        public string CompanyEmailAddress { get; private set; }
        public string ManagerEmailAddress { get; private set; }
        public string CompanyWebsiteAddress { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        public string StoreAddress { get; private set; }       
        public string AddressOne { get; private set; }
        public string AddressTwo { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public int PinCode { get; private set; }

        public User() : base(Guid.NewGuid())
        {
        }

        public User(string firstName, string lastName, int mobileNumber, string emailAddress)
        {
            FirstName = firstName;
            LastName = lastName;
            ConstructUserName(firstName, lastName);
            MobileNumber = mobileNumber;
            EmailAddress = emailAddress;
        }

        private void ConstructUserName(string firstName, string lastName)
        {
            int counter = 0;
            while (UserNameExists(firstName, lastName, counter))
            {
                if(counter == 0)
                {
                    UserName = firstName + lastName;
                }
                else
                {
                    UserName = firstName + lastName + counter.ToString();
                }          
                counter++;
            }
        }

        private bool UserNameExists(string firstName, string lastName, int counter)
        {
            //TO DO: Check in database if username exists 
            return false;
        }

        public void UpdateUserDetails(UserType userType, string firstName, string lastName, 
                                        int mobileNumber, string companyName, string companyAddress, string storeAddress,
                                        string companyEmailAddress, string managerEmailAddress, string companyWebsite,
                                        string emailAddress, DateTime dateOfBirth, string addressOne, string addressTwo,
                                        string city, string state, int pinCode)
        {
            switch (userType)
            {
                case UserType.JobProvider:
                    if (!HasFulfilledJobProvider(firstName, lastName, mobileNumber, companyEmailAddress
                                                    , managerEmailAddress, companyWebsite, companyAddress))
                    {
                        throw new InvalidOperationException("Please fill all the required fields");
                    }
                    break;
                case UserType.ServiceProvider:
                case UserType.ProductInventoryRepresentative:
                    if (!HasFulfilledServiceProvider(firstName, lastName, mobileNumber, emailAddress
                                                    ,storeAddress, city, state, pinCode))
                    {
                        throw new InvalidOperationException("Please fill all the required fields");
                    }
                    break;
                case UserType.JobApplicant:
                    if (!HasFulfilledProductJobApplicant(firstName, lastName, mobileNumber, emailAddress
                                                    , dateOfBirth, addressOne, addressTwo, city, state, pinCode))
                    {
                        throw new InvalidOperationException("Please fill all the fields");
                    }
                    break;

                case UserType.ServiceConsumer:
                case UserType.ProductCustomer:
                    if (!IsQualifiedForCustomer(firstName, lastName, mobileNumber, emailAddress
                                                    , addressOne, addressTwo, city, state, pinCode))
                    {
                        throw new InvalidOperationException("Please fill all the fields");
                    }
                    break;
                default:
                    break;
            }
        }

        private bool IsQualifiedForCustomer(string firstName, string lastName, int mobileNumber, string emailAddress, string addressOne, string addressTwo, string city, string state, int pinCode)
        {
            if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) || string.IsNullOrEmpty(mobileNumber.ToString()) ||
                string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(addressOne) || string.IsNullOrEmpty(addressTwo) ||
                string.IsNullOrEmpty(city) || string.IsNullOrEmpty(state) || string.IsNullOrEmpty(pinCode.ToString()))
            {
                return false;
            }
            else
            {
                FirstName = firstName;
                LastName = lastName;
                MobileNumber = mobileNumber;
                EmailAddress = emailAddress;
                AddressOne = addressOne;
                AddressTwo = addressTwo;
                City = city;
                State = state;
                PinCode = pinCode;
                return true;
            }           
        }

        private bool HasFulfilledProductJobApplicant(string firstName, string lastName, int mobileNumber, string emailAddress, DateTime dateOfBirth, string addressOne, string addressTwo, string city, string state, int pinCode)
        {
            if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) || string.IsNullOrEmpty(mobileNumber.ToString()) ||
                string.IsNullOrEmpty(dateOfBirth.ToString()) || string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(addressOne) ||
                string.IsNullOrEmpty(addressTwo) || string.IsNullOrEmpty(city) || string.IsNullOrEmpty(state) ||
                string.IsNullOrEmpty(pinCode.ToString()))
            {
                return false;
            }
            else
            {
                FirstName = firstName;
                LastName = lastName;
                MobileNumber = mobileNumber;
                EmailAddress = emailAddress;
                DateOfBirth = dateOfBirth;
                AddressOne = addressOne;
                AddressTwo = addressTwo;
                City = city;
                State = state;
                PinCode = pinCode;
                return true;
            }
        }

        private bool HasFulfilledServiceProvider(string firstName, string lastName, int mobileNumber, string emailAddress, string storeAddress, string city, string state, int pinCode)
        {
            if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) || string.IsNullOrEmpty(mobileNumber.ToString()) ||
                string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(storeAddress) || string.IsNullOrEmpty(city) || string.IsNullOrEmpty(state) ||
                string.IsNullOrEmpty(pinCode.ToString()))
            {
                return false;
            }
            else
            {
                FirstName = firstName;
                LastName = lastName;
                MobileNumber = mobileNumber;
                EmailAddress = emailAddress;
                StoreAddress = storeAddress;
                City = city;
                State = state;
                PinCode = pinCode;
                return true;
            }
        }

        private bool HasFulfilledJobProvider(string firstName, string lastName, int mobileNumber, string companyEmailAddress, string managerEmailAddress, string companyWebsite, string companyAddress)
        {
            if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) || string.IsNullOrEmpty(mobileNumber.ToString()) ||
                string.IsNullOrEmpty(companyEmailAddress) || string.IsNullOrEmpty(managerEmailAddress) || string.IsNullOrEmpty(companyWebsite) || string.IsNullOrEmpty(companyAddress))
            {
                return false;
            }
            else
            {
                FirstName = firstName;
                LastName = lastName;
                MobileNumber = mobileNumber;
                CompanyEmailAddress = companyEmailAddress;
                ManagerEmailAddress = managerEmailAddress;
                CompanyWebsiteAddress = companyWebsite;
                CompanyAddress = companyAddress;
                return true;
            }
        }
    }
}
