﻿using HFifty.Classified.SharedKarnel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFifty.Classified.JobPortal.Domain
{
    public class JobApplicationCreatedNotifier : IDomainEvent
    {
        public JobApplication JobApplication { get; set; }
        public JobProvider Provider { get; set; }
    }
}
